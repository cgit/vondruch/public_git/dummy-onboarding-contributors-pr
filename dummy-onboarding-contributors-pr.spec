Name: dummy-onboarding-contributors-pr
Version: 1
Release: %autorelease
Summary: Onboarding package - CONTRIBUTORS pull request
License: MIT
Source0: CONTRIBUTORS
BuildArch: noarch

%description
This is Onboarding package. The intent is just to help onboard new Fedora
maintainers. This package should not be installed (but when installed it should
not cause any harm).

%prep
%setup -T -c %{name}-%{release}

cp %{SOURCE0} .

%build

%install

%check

%files
%doc CONTRIBUTORS

%changelog
%autochangelog
